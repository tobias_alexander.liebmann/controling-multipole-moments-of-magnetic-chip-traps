# Controlling multipole moments of magnetic chip traps
This project exists for the examination of the magnetic induction field exerted by piecewise 
straight wires. The calculations are done using the Biot-Savart law. In this case it is 
represented by a sum of the magnetic induction field of straight wires
```math
\vec{B}\left(\vec{r}\right)\approx\frac{\mu_{0}I}{4\pi}\sum\limits_{i=1}^{N}\frac{\left(
\vec{e}_{i+1}-\vec{e}_{i}\right)\cdot\vec{w}_{i}}{r_{i}\left(1-\left(\vec{e}_{i}
\cdot\vec{w}_{i}\right)^{2}\right)}\left(\vec{e}_{i}\times\vec{w}_{i}\right), \quad 
\vec{w}_{i}=\frac{\vec{r}_{i+1}-\vec{r}_{i}}{|\vec{r}_{2}-\vec{r}_{1}|}, \;
\vec{e}_{i}=\frac{\vec{r}-\vec{r}_{i}}{|\vec{r}-\vec{r}_{i}|},
```
where $`\vec{B}`$ is the magnetic induction field, $`\vec{r}`$ is the position of the observer, 
$`\vec{r}_{i}`$ is the $`i`$-th corner point of the piecewise straight wire, $`\mu_{0}`$ is the 
magnetic constant and $`I`$ the current flowing through the wire.

## Installation
The package can be easily installed with `pip`. To do this change to the directory where the `setup.py` file of 
this package is located and run
```bash
$ pip install .
```
in the command line.

## Usage
The basic usage of this package consists of creating piecewise straight wires and examining 
their magnetic induction fields. The examination of these fields is achieved via a `BFieldAnaylzer` class which has the 
following features:
- Calculation of the minimum position of $`\vec{B}`$.
- Calculating the hessian of $`\vec{B}`$.
- Calculation of the multipole moments of $`\vec{B}`$.

### Creating wires
To create a `Wire` object you have to at least provide the constructor with the corner points 
of the wire and a current which flows through the wire. The wire and its current are described in SI units by default.

```python
import numpy as np
from magopt.magnetic_field_calculation import Wire

current = 1  # Current in Ampere.
wire_points = np.array([[0, 0, -1], [0, 0, 1]])  # Wire points in meters.
my_wire = Wire(wire_points, current)
```

If you don't want to use SI units, you can change the following scales by entering them in the 
constructor:
- `wire_length_scale`: The length scale of the wire points (default are meters).
- `observer_length_scale`: The length scale for observers (default are meters)
- `current_scale`: The unit used for the current (default is Ampere)
- `b_field_scale`: Units used to describe the magnetic induction field (default is Tesla)

All of the above parameters can be passed to the constructor as default value parameters. The scale reverse to the 
scaling relative to the SI units. For example if you want to use millimeters instead of meters to describe the 
corner points of the wire, you can use the following code

```python
my_wire = Wire(wire_point, current, wire_length_scale=1e-3)
```

You can also do this for all the other scales from the list above.

### Calculating magnetic induction fields
To calculate the magnetic induction of a `Wire` object one simply has to call the `calculate_b_field_at` method

```python
position = np.array([1., 1., 1.])
b_field = my_wire.calculate_b_field_at(position)
```

You can also call this method with multiple positions

```python
positons = np.array([[1., 1.], [1., -1.], [-1., -1.]])
b_field = my_wire.calculate_b_field_at(positons)
```
where the first row contains all the x-coordinates of all observers and the second and third row all the y- and 
z-coordinates.

### Analyzing magnetic induction fields

The `BFieldAnalyzer` class allows for the examination of magnetic induction fields $`\vec{B}`$ of all kinds. You have
to provide an instance of this class in the constructor with $`\vec{B}`$ and its magnitude $`|\vec{B}|`$ in the form of
Python functions.

```python
def b_field(position: np.ndarray) -> np.ndarray:
    return position**2

def magnitude_of_b(position: np.ndarray) -> np.ndarray:
    return np.linalg.norm(b_field(position))

anaylzer = BFieldAnalyzer(b_field, magnitude_of_b)
```
Note that the fields have to be written in such a way that both accept numpy arrays of shape (3,), where the entries
correspond to the x-,y- and z-coordinates of the observer. Additionally, the function representing $`\vec{B}`$ must be
able to take in numpy arrays of shape (3, N), where is a arbitrary natural number. In these arrays the first row
represents the x-coordinates of all observers, the second and third row represent the y- and z-coordinates of all
observers.

## Benchmark
A benchmark to measure the performance when calculating magnetic induction fields can be found below. The hardware used 
in this benchmark was Intel Core i7-1165G7 with 8 threads and 16 GB RAM. The image 
was recorded using a wire with $`101`$ random corner points (which equates to $`100`$ individual straight wires). 
The corner points were chosen randomly in a cube with side length $`2\;\mathrm{m}`$ around the origin (which is the 
midpoint of the cube). The benchmark is calculated for differing number of observers from $`100`$ to $`1000`$, whose 
location is chosen randomly in a cube with side length $`2\;\mathrm{m}`$ with its midpoint at 
$`(2.1\;\mathrm{m})\vec{e_{z}}`$. This is done to ensure that no observer lies on a wire connecting two corner points, 
which would result in an infinite field strength.
![Benchmark](/docs/images/benchmark.png)

### Additional documentation
For additional documentation visit 
[readthedocs](https://controlling-multipole-moments-of-magnetic-chip-traps.readthedocs.io/en/latest/).
The package also provides several examples in the `examples/` subdirectory, which may help you.

## Authors
The authors which contributed to this project are:
- Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de

## Support
If you have problems using the software or found a bug, you can either open an issue or write an e-mail to the following
e-mail address: tobias_alexander.liebmann@tu-darmstadt.de

"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 20.12.2021

This file produces a benchmark of the calculate_b_field_at method of the Wire class. It produces a plot showing the
number of observation points against the calculation time in milliseconds. The orientation of the wires used for the
calculation and the observation points are both chosen randomly, but in such a way that it is not possible for an
observer to lay on a wire.
"""

import numpy as np
import matplotlib.pyplot as plt
from magopt.magnetic_field_calculation import Wire
from time import perf_counter


# Define the wire points
number_of_wire_points = 101
number_of_wires = number_of_wire_points - 1
wire_points = np.random.rand(number_of_wire_points, 3)
# The current flowing through the wire in Ampere.
current = 1
benchmark_wire = Wire(wire_points, current, b_field_scale=1e-4)
# Defines the numbers of observers used for the benchmark.
number_of_observers = np.linspace(100, 1000, num=40, dtype=int)
# Define the midpoint of the observers.
# The observers are lifted above the wire to ensure that no observer is located on a wire, which would result in an
# infinitely strong field.
observers_mid_point = np.array([[0.0], [0.0], [2.1]])
# Array to save the timings in.
timings = np.array([])

for num_of_obs in number_of_observers:
    # The 2.1 is added in the end to make sure that no observer lies on a wire.
    observers = np.random.rand(3, num_of_obs) + observers_mid_point
    # Do the benchmark
    start_time = perf_counter()
    benchmark_wire.calculate_b_field_at(observers)
    end_time = perf_counter()
    # calculate the elapsed time in ms.
    elapsed_time_in_ms = 1000*(end_time-start_time)
    timings = np.append(timings, elapsed_time_in_ms)

# Plot the results
plt.plot(number_of_observers, timings, marker=".")
plt.grid()
plt.title(f"Benchmark for the calculation of the B-field for {number_of_wires} wires")
plt.xlabel("number of observers")
plt.ylabel("calculation time in ms")
plt.show()

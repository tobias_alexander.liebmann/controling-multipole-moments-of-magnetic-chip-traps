# Changelog
This changelog contains the changes from each version to another.

## v0.1.0
Added features:
- Included a `Wire` class, which allows for the calculation of the magnetic induction field of piecewise straight wires

## v0.2.0
Added features:
- A `BFiledAnaylzer` class which has the following features:
  - The calculation of the minimum position of the magnitude of magnetic induction fields $`|\vec{B}|`$.
  - The calculation of the hessian of $`|\vec{B}|`$ at a given position.
  - The calculation of the multipole moment of $`\vec{B}`$.

Changed features:
- To calculate the magnetic induction field of multiple observers using the `calculate_b_field_at` method of the `Wire` 
class one has to provide a numpy array of shape (3, N) which has previously been an array of shape (N, 3).
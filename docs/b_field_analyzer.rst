Magnetic field analyzer class
=============================

.. autoclass:: magnetic_field_analyzer.BFieldAnalyzer
    :members:

    .. automethod:: __init__
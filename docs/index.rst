.. magopt documentation master file, created by
   sphinx-quickstart on Fri Dec 31 14:27:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to magopt's documentation!
==================================

.. automodule:: __init__

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   wire
   b_field_analyzer


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

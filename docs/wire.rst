Wire class
==========

.. autoclass:: magnetic_field_calculation.Wire
    :members:

    .. automethod:: __init__
"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de

This package contains modules for the examination of the magnetic induction field created by a piecewise straight wire,
as well as saving and loading pickled objects. It also has an examples/ subdirectory which you should check out.

This package includes the following modules.

:magnetic_field_calculation: Includes the definition of a :code:`Wire` class to examine the B-field of a piecewise
    straight wire and functions to load and save wires in pickled form.

:magnetic_field_analyzer: Includes a :code:`BFieldAnalyzer` class which allows for examination of different features of a
    magnetic induction field.
"""

__version__ = "0.1.0"
__project__ = "magopt"
__author__ = "Tobias Liebmann"

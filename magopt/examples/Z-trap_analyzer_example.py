"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 19.01.2021

Example using the Wire and BFieldAnalyzer class together.
"""

from magopt.magnetic_field_calculation import Wire
from magopt.magnetic_field_analyzer import BFieldAnalyzer
import numpy as np
from time import perf_counter

# Characterize the wire.
current = 1
# The following wire points define an example of a Z-trap.
wire_points = np.array([[-1, -10, 0], [-1, 0, 0], [1, 0, 0], [1, 10, 0]])
# The units chosen are Gauss for the magnetic induction field and millimeters for the length scales.
example_wire = Wire(wire_points, current, b_field_scale=1e-4, observer_length_scale=1e-3, wire_length_scale=1e-3)
external_field = np.array([0, 2, 0])


# Calculate the magnetic induction field and its magnitude with the following functions.
def b_field(position: np.ndarray) -> np.ndarray:
    """
    The magnetic induction field of a simple Z-trap with a straight wire along the x-axis reaching from x=-1 mm and
    x=1 mm. The field of the wire configuration is superposed by an external field of 2 G in y-direction.

    :param position: A numpy array of shape (3, N) defining the position or multiple positions of observers.
    :return: A numpy array of shape (3, N) representing the magnetic induction field at the given positions.
    """
    return example_wire.calculate_b_field_at(position) + external_field[:, np.newaxis]


def magnitude_of_b(position: np.ndarray) -> np.ndarray:
    """
    The magnitude of the B-field of a simple Z-trap with a straight wire along the x-axis reaching from x=-1 mm and
    x=1 mm. The field of the wire configuration is superposed by an external field of 2 G in y-direction.

    :param position: A numpy array of shape (3,) representing a position in 3D space.
    :return: The magnitude of the B-field at the given position.
    """
    field = example_wire.calculate_b_field_at(position) + external_field
    return np.linalg.norm(field)


# Define the analyzer object
analyzer = BFieldAnalyzer(b_field, magnitude_of_b)
# Calculate minimum position
minimum_position, minimum_value, _ = analyzer.find_minimum(np.array([0., 0., 1.]))

# Calculate multipole moment
l = 3
m = 1
integration_radius = 0.1  # Integration radius in mm.
start = perf_counter()
multipole_moment = analyzer.calculate_multipole_moment(l, m, integration_radius, minimum_position)
end = perf_counter()

# Print the results
print(f"The minimum position is located at: {minimum_position} mm.")
print(f"The multipole moment l={l}, m={m} has the value {multipole_moment:.5f}.")
print(f"The calculation time for the multipole moment took {end - start} s.")

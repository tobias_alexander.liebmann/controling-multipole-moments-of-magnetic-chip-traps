"""
:Author: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 20.01.2021

This is an example file which depicts the magnitude of the magnetic induction field, B of a simple Z-trap. B is
depicted at the minimum position in the x-y-plane, the x-z-plane and the y-z-plane.
"""

import numpy as np
from magopt.magnetic_field_calculation import Wire
from magopt.magnetic_field_analyzer import BFieldAnalyzer
import matplotlib.pyplot as plt

# Characterize the wire.
current = 1
wire_points = np.array([[-1, -100, 0], [-1, 0, 0], [1, 0, 0], [1, 100, 0]])
# The units chosen are Gauss for the magnetic induction field and millimeters for the length scales.
example_wire = Wire(wire_points, current, b_field_scale=1e-4, observer_length_scale=1e-3, wire_length_scale=1e-3)
external_field = np.array([0, 2, 0])  # External field of 2 G in y direction.


# Calculate the magnetic induction field and its magnitude with the following functions.
def b_field(position: np.ndarray) -> np.ndarray:
    """
    The magnetic induction field of a simple Z-trap with a straight wire along the x-axis reaching from x=-1 mm and
    x=1 mm. The field of the wire configuration is superposed by an external field of 2 G in y-direction.

    :param position: A numpy array of shape (3, N) defining the position or multiple positions of observers.
    :return: A numpy array of shape (3, N) representing the magnetic induction field at the given positions.
    """
    return example_wire.calculate_b_field_at(position) + external_field[:, np.newaxis]


def magnitude_of_b(position: np.ndarray) -> np.ndarray:
    """
    The magnitude of the B-field of a simple Z-trap with a straight wire along the x-axis reaching from x=-1 mm and
    x=1 mm. The field of the wire configuration is superposed by an external field of 2 G in y-direction.

    :param position: A numpy array of shape (3,) representing a position in 3D space.
    :return: The magnitude of the B-field at the given position.
    """
    if len(position.shape) == 1:
        field = example_wire.calculate_b_field_at(position) + external_field
        return np.linalg.norm(field)
    else:
        field = example_wire.calculate_b_field_at(position) + external_field[:, np.newaxis]
        return np.linalg.norm(field, axis=0)


# Define the analyzer object
analyzer = BFieldAnalyzer(b_field, magnitude_of_b)
# Calculate minimum position
minimum_position, minimum_value, _ = analyzer.find_minimum(np.array([0., 0., 1.]))
deviation = np.array([0.2, 0.2, 0.2])
matrix_dimension = 32
observers = np.linspace(minimum_position - deviation, minimum_position + deviation, matrix_dimension).T

# Define the matrices which will be used for the contour plots.
x_mat = np.tile(observers[0], (matrix_dimension, 1))
y_mat = np.tile(observers[1], (matrix_dimension, 1))
z_mat = np.tile(observers[2], (matrix_dimension, 1))

#################################
# Make the plots in the x-y-plane
#################################

# Calculate the positions and the magnitude of the magnetic induction field.
x_y_plane_coordinates_x = np.tile(observers[0], matrix_dimension)
# I have to flip these values since the grid values start in the upper left corner which corresponds to positive
# values of y.
x_y_plane_coordinates_y = np.repeat(np.flip(observers[1]), matrix_dimension)
x_y_plane_coordinates_z = np.repeat(minimum_position[2], matrix_dimension ** 2)
x_y_plane_positions = np.vstack((x_y_plane_coordinates_x, x_y_plane_coordinates_y, x_y_plane_coordinates_z))
mag_b_x_y_plane = magnitude_of_b(x_y_plane_positions).reshape(matrix_dimension, matrix_dimension)

# Make the plot.
fig1 = plt.figure(1)
ax1 = plt.axes()
ax1.set_xlabel("x in mm")
ax1.set_ylabel("y in mm")
ax1.set_title(rf"$|\vec{{B}}(x,y,z_{{\mathrm{{min}}}})|$ of a Z-trap in G, "
              rf"$z_{{\mathrm{{min}}}}={minimum_position[2]:.2f}$ mm")
contour_plot_x_y = ax1.contourf(x_mat, y_mat.T, mag_b_x_y_plane)
fig1.colorbar(contour_plot_x_y)

#################################
# Make the plots in the x-z-plane
#################################

# Calculate the positions and the magnitude of the magnetic induction field.
x_z_plane_coordinates_x = np.tile(observers[0], matrix_dimension)
x_z_plane_coordinates_y = np.repeat(minimum_position[1], matrix_dimension ** 2)
x_z_plane_coordinates_z = np.repeat(observers[2], matrix_dimension)
x_z_plane_positions = np.vstack((x_z_plane_coordinates_x, x_z_plane_coordinates_y, x_z_plane_coordinates_z))
mag_b_x_z_plane = magnitude_of_b(x_z_plane_positions).reshape(matrix_dimension, matrix_dimension)

# Make the plot.
fig2 = plt.figure(2)
ax2 = plt.axes()
ax2.set_xlabel("x in mm")
ax2.set_ylabel("z in mm")
ax2.set_title(rf"$|\vec{{B}}(x,y_{{\mathrm{{min}}}},z)|$ of a Z-trap in G, "
              rf"$y_{{\mathrm{{min}}}}={int(minimum_position[1])}$ mm")
contour_plot_x_z = ax2.contourf(x_mat, z_mat.T, mag_b_x_z_plane)
fig2.colorbar(contour_plot_x_z)

#################################
# Make the plots in the y-z-plane
#################################

# Calculate the positions and the magnitude of the magnetic induction field.
y_z_plane_coordinates_x = np.repeat(minimum_position[0], matrix_dimension ** 2)
y_z_plane_coordinates_y = np.tile(observers[1], matrix_dimension)
y_z_plane_coordinates_z = np.repeat(observers[2], matrix_dimension)
y_z_plane_positions = np.vstack((y_z_plane_coordinates_x, y_z_plane_coordinates_y, y_z_plane_coordinates_z))
mag_b_y_z_plane = magnitude_of_b(y_z_plane_positions).reshape(matrix_dimension, matrix_dimension)

# Make the plot.
fig3 = plt.figure(3)
ax3 = plt.axes()
ax3.set_xlabel("y in mm")
ax3.set_ylabel("z in mm")
ax3.set_title(rf"$|\vec{{B}}(x_{{\mathrm{{min}}}},y,z)|$ of a Z-trap in G, "
              rf"$x_{{\mathrm{{min}}}}={int(minimum_position[0])}$ mm")
contour_plot_y_z = ax3.contourf(y_mat, z_mat.T, mag_b_y_z_plane)
fig3.colorbar(contour_plot_y_z)
plt.show()

"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 19.01.2021

An example using the BFieldAnalyzer class to analyze a simple magnetic induction field.
"""

import numpy as np
from magopt.magnetic_field_analyzer import BFieldAnalyzer


def b_field(position: np.ndarray) -> np.ndarray:
    """
    Calculates the magnetic induction at a given position.

    :param position: A position in 3D space.
    :return: The magnetic induction field.
    """
    return position**2


def magnitude_of_b(position: np.ndarray) -> np.ndarray:
    """
    Magnitude of the magnetic induction field defined from above.

    :param position: A position in 3D space.
    :return: The magnitude of the magnetic induction field.
    """
    return np.linalg.norm(b_field(position))


analyzer = BFieldAnalyzer(b_field, magnitude_of_b)
minimum_position, _, _ = analyzer.find_minimum(np.array([1., 1., 1.]))
hessian = analyzer.calculate_hessian_at(minimum_position)
multipole_moment_3_1 = analyzer.calculate_multipole_moment(3, 1, 1, minimum_position)

print(f"|B| has its minimum at {minimum_position} m")
print(f"The Hessian in the minimum is given by:\n {hessian} in G/m^2")
print(f"The multipole moment of order l=3, m=1 is given by {multipole_moment_3_1}.")

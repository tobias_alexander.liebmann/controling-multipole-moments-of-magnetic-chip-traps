"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 23.12.2021

This example file demonstrates how to load pickled Wire objects and plot its magnetic induction field.
"""

import magopt.magnetic_field_calculation as mfc
import numpy as np
import matplotlib.pyplot as plt

example_wire = mfc.load("fixtures/example_wire.pkl")

observers = np.linspace((0.1, 0, 0), (5, 0, 0), 20).T
# Calculate the magnetic induction field.
b_field = example_wire.calculate_b_field_at(observers)

# Plot the example.
x = observers[0]
y = b_field[1]
plt.plot(x, y)
plt.xlabel(rf"$x$ in {example_wire.observer_length_scale:.1e}$\cdot$m")
plt.ylabel(rf"$B_{{z}}$ in {example_wire.b_field_scale:.1e}$\cdot$T")
plt.show()

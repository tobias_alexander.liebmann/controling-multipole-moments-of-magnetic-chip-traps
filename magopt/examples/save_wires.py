"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 23.12.2021

This example file demonstrates how to save pickled Wire objects.
"""

import magopt.magnetic_field_calculation as mfc
import numpy as np

# Set the B field to Gauss.
b_field_scale = 1e-4
# Characterize the wire.
current = 1
# The following wire points define a straight wire starts at z = -1 m and ends at z = 1 m.
wire_points = np.array([[0, 0, -1], [0, 0, 1]])
example_wire = mfc.Wire(wire_points, current, b_field_scale=b_field_scale)

# Save the wire
mfc.save(example_wire, "fixtures/example_wire.pkl")

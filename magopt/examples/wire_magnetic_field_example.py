"""
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de
:Date of origin: 17.12.2021

This is an example file showing how to calculate the magnetic induction field of an instance of the Wire class.
"""

import numpy as np
from magopt.magnetic_field_calculation import Wire
import matplotlib.pyplot as plt

# Characterize the wire.
current = 1
# The following wire points define a straight wire starts at z = -1 m and ends at z = 1 m.
wire_points = np.array([[0, 0, -1], [0, 0, 1]])
example_wire = Wire(wire_points, current, b_field_scale=1e-4)

# Calculate the magnetic induction field.
observers = np.linspace((0.1, 0, 0), (5, 0, 0), 20).T
b_field = example_wire.calculate_b_field_at(observers)

# Plot the example.
x = observers[0]
y = b_field[1]
plt.plot(x, y)
plt.title(f"B-field of a finite straight wire reaching from z=-1 m to z=1 m")
plt.xlabel(f"$x$ in m")
plt.ylabel(f"$B_{{z}}$ in G")
plt.show()

"""
:Date of origin: 04.01.2022
:Authors: Tobias Liebmann, tobias_alexander.liebmann@tu-darmstadt.de

This file contains the definition of a BFieldAnalyzer object, which allows one to calculate the minimum of the
magnitude of a magnetic induction field, B, the eigensystem of the magnitude of B and the corresponding multipole
decomposition at a given position.

:BFieldAnalyzer: Class which analyzes several properties of a magnetic induction field.
"""
import numpy as np
from scipy import optimize as opt
from scipy import special as spc
import numdifftools as ndt
import quadpy as qp


class BFieldAnalyzer:
    """
    This class defines a method for the analysis of magnetic induction fields in 3D space. You pass a function
    representing the magnetic induction field and a function representing the magnitude of the associated B-field to
    this object. This objects allows you to analyse several of the B-fields properties using the following methods and
    attributes:

    - b_field (attribute/method): Function object passed to the constructor representing the magnetic induction field.
    - magnitude_of_b (attribute/method): Function object representing the magnitude of the magnetic induction field.
    - find_minimum (method): Returns the minimum of the magnitude of the magnetic induction field in 3D space.
    - find_minimum_along_z_axis (method): Calculates the minimum position only along the z-axis.
    - calculate_hessian_at (method): Calculates the hessian of the magnitude of the magnetic induction field at a given
      position.
    - calculate_eigensystem (method): Calculates the eigenvectors and eigenvalues of a matrix.
    - calculate_multipole_moment (method): Calculates the multipole moment of order l and m at a given position.
    """

    def __init__(self, b_field: callable, magnitude_of_b: callable):
        """
        Constructor of the magnetic field analyzer. It takes a magnetic induction field (function or method object) and
        the associated magnitude as an input.

        :param b_field: The magnetic induction field, which is examined. This function should be able to take numpy
            arrays with shape (3, N) as input (N is an arbitrary integer) and return a numpy array of shape (3, N).
        :param magnitude_of_b: The magnitude of magnetic induction field, which is examined. This function should take
            in a numpy array of shape (3,) and return a float.
        """
        self.b_field = b_field
        self.magnitude_of_b = magnitude_of_b

    def find_minimum(self, starting_position: np.ndarray) -> tuple:
        """
        Uses the minimize function by scipy optimize to calculate the minimum of the magnitude of the magnetic
        induction field.

        :param starting_position: The first point in 3D space at which the algorithm starts.
        :return: A tuple containing:

            1. The minimum position (x0, y0, z0) in 3D space.
            2. The value of the function at the minimum position.
            3. A boolean indicating if the minimization was a success or not.
        """
        result = opt.minimize(self.magnitude_of_b, x0=starting_position)
        return result.x, result.fun, result.success

    def _magnitude_of_b_z_component(self, z: float):
        """
        This is a helper function to calculate the minimum of the magnitude of the B_field in z-direction. It
        calculates the magnitude of the B-field on the z-axis.

        :param z: Position at which the magnitude of the B-field is calculated.
        :return: The magnitude of the B-field at the given position.
        """
        # The z value has to be cast as a float to ensure numpy does not complain.
        z = float(z)
        position = np.array([0., 0., z])
        return self.magnitude_of_b(position)

    def find_minimum_along_z_axis(self, starting_point: float) -> tuple:
        """
        Uses the minimize function from scipy.optimize to calculate the minimum of the magnitude of the B-field along
        the z-axis.

        :param starting_point: Starting position of the algorithm.
        :return: A tuple containing:

            1. The minimum position along the z-axis.
            2. The value of the function at the minimum position.
            3. A boolean indicating if the minimization was a success or not.

        """
        result = opt.minimize(self._magnitude_of_b_z_component, x0=np.array([starting_point]))
        return result.x[0], result.fun, result.success

    def calculate_hessian_at(self, position: np.ndarray) -> np.ndarray:
        """
        Uses the Hessian method of the numdifftools package to calculate the Hessian of the magnitude of the B-field
        at a given position.

        :param position: 1D NumPy array defining a position in 3D space at which the Hessian is examined.
        :return: A numpy array with shape (3, 3) representing the Hessian of the magnitude of the magnetic induction
            field at the given position.
        """
        hessian = ndt.Hessian(self.magnitude_of_b)(position)
        return hessian

    @staticmethod
    def calculate_eigensystem(matrix: np.ndarray) -> tuple:
        """
        Calculates the Eigensystem of a complex hermitian or real symmetric matrix.

        :param matrix: The matrix of which the eigensystem is calculated.
        :return: A tuple consisting of the eigenvalues and eigenvectors.
        """
        return np.linalg.eigh(matrix)

    def calculate_multipole_moment(self, l: int, m: int, integration_radius: float, int_sphere_midpoint: np.ndarray,
                                   scheme=qp.u3.get_good_scheme(13)) -> float:
        r"""
        Calculates the multipole moment of a certain order (given by l and m) using the provided integration
        radius around a given position. The multipole moments are calculated using the equation

        .. math ::
            \Phi_{lm}(R)=\frac{1}{l}\int\limits_{\mathrm{S}_{1}}Y^{*}_{lm}
            (\theta,\varphi )B_{r}(R, \theta, \varphi)\mathrm{d}^{2}\Omega,

        where :math:`\Phi_{lm}` are the multipole moments, :math:`Y_{lm}(\theta,\varphi)` are the spherical harmonics
        , :math:`B_{r}` is the radial part of the B-field in spherical coordinates, and :math:`R` is the integration
        radius.

        :param int_sphere_midpoint: Midpoint of the integration in 3D space given by numpy array with shape (3,).
        :param scheme: Integration scheme used to calculate the multipole moments.
        :param l: Angular momentum number of the spherical harmonics :math:`Y_{lm}(\theta, \varphi)`. :math:`l` is a
            natural number, defaults to quadpy.u3.get_good_scheme(13)
        :param m: Magnetic quantum number of the spherical harmonics :math:`Y_{lm}(\theta, \varphi)`. :math:`m` obeys
            the restriction :math:`-l \leq m \leq l`.
        :param integration_radius: The radius of the integration sphere :math:`R` given in the same units as the
            observers of the magnetic induction field.
        :return: The multipole moment :math:`\Phi_{lm}(R)`.
        """

        def integrand(theta_phi: np.ndarray) -> float:
            """
            This is a helper function used to calculate the integrand for the numerical integration.

            :param theta_phi: Tuple consisting of the angles of the spherical coordinate system theta and phi.
            :return: The value of the integrand defined by the angles theta and phi.
            """
            theta, phi = theta_phi
            # This represents the radial unit vector in spherical coordinates.
            radial_unit_vector = np.array([np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)])
            # This is the examination point used for the integral over the unit sphere.
            # The np.newaxis is needed because quadpy inputs the angles for the calculation of the integral in the
            # form of arrays and because of this the shape of the radial_unit_vector is now 2D, while
            # int_sphere_midpoint is 1D, which means they can't be added.
            coordinates = integration_radius * radial_unit_vector + int_sphere_midpoint[:, np.newaxis]
            # The magnetic field described for an observer in radial coordinates. Note that the components of the
            # B-field are still in cartesian coordinates.
            b_field_radial_part = np.sum(self.b_field(coordinates)*radial_unit_vector, axis=0)
            # This is the function that has to be integrated to calculate the multipole moments.
            func_to_integrate = np.conj(spc.sph_harm(m, l, phi, theta)) * b_field_radial_part
            return func_to_integrate

        # Integrate to get the multipole moment.
        return scheme.integrate_spherical(integrand)/l

"""
:Date of origin: 15.12.2021
:Authors: Tobias Liebmann, tobias-alexander.liebmann@tu-darmstadt.de

This file includes the definition of a wire object which represents an arbitrary 1D wire structure in 3D space. This
wire is used to calculate its magnetic induction field. Further, it also contains two functions for loading and saving
objects using pickle.

:Wire: Class representing a piecewise straight wire in 3D space.
:save: Saves a pickled object to specified file.
:load: Loads a pickled object from a specified file.
"""

import numpy as np
from scipy.constants import mu_0, pi
import pickle as pkl


class Wire:
    """
    This class represents an infinitesimally thin wire in 3D space, which is made up of piecewise straight wires.
    All units used for and by the wire are SI units by default. You can scale these units with the appropriate default
    value parameters in the constructor to describe the wire in the units you want. The piecewise straight wire is
    defined by a numpy array of shape (3, N) where N is an arbitrary integer number. This array defines all the corners
    of the wire. For example the array

    >>> np.array([[0, 0, -1], [0, 0, 1]])

    defines a wire reaching from -1 m to 1 m in z-direction. The purpose of this wire object is calculation of its
    magnetic induction field. The :code:`Wire` class includes the following attributes and methods:

    - calculate_b_field_at (method): Calculates the magnetic induction field at one position or multiple positions.
    - wire_points (attribute): A numpy array defining all the corner points of the piecewise straight wire.
    - current (attribute): Current flowing through the wire.
    - wire_length_scale (attribute): Defines the units in which the wire points are defined (default is meter).
    - observer_length_scale (attribute): Defines the units of an observer examining the B-field (default is meter).
    - current_scale (attribute): Defines the units of the current (default is Ampere).
    - b_field_scale (attribute): Defines the units used for the magnetic induction field (default is Tesla).
    """
    def __init__(self, wire_points: np.ndarray, current: float, wire_length_scale: float = 1,
                 observer_length_scale: float = 1, current_scale: float = 1, b_field_scale: float = 1) -> None:
        """
        This is the constructor of the wire object. This object represents a wire consisting of piecewise straight
        wires, which are all connected. The parameters wire_points is a numpy array with shape (n, 3) and  describes the
        edges of the straight wires in 3D space. wire_points = np.array([[-1, 0, 0], [1, 0, 0]]) for example describes
        a straight wire along the x-axis reaching from -1 to 1 in units specified by the wire_length_scale.
        The wire also has a current flowing through it. The default units used for the wire, current, B-field and
        position of the observer (when calculating the B-field) are the SI units. You can change these units via the
        scale parameters.

        :param wire_points: This is numpy array with the shape (n, 3) where n>=2. It represents the edges of
            the piecewise straight wires which make up the whole wire. The units of this parameter are specified using
            the wire_length_scale parameter. The default is meters.
        :param current: The current that flows through the wire its units can be specified using the current_scale
            parameter the default unit is Ampere.
        :param wire_length_scale: Describes the length scale for the wire points in meters. For example, if you want to
            describe the wire points in millimeter you enter 1e-3, defaults to 1.
        :param observer_length_scale: Describes the length scale for the observer of the B-field in meters.
            For example, if you want to examine the B-field on a centimeter scale, you enter 1e-2, defaults to 1.
        :param current_scale: Describes the scale of the current flowing the through the wire. The default is Ampere.
            If you wanted a scale of Milli-ampere, you need to enter 1e-3, defaults to 1.
        :param b_field_scale: Describes the scale of the B-field. The default scale is Tesla. If you wanted Gauss as
            units of the B-field, you have to enter 1e-4, defaults to 1.
        """
        # This attribute depends on the wire points and is implicitly set in the property of the wire points.
        self._wire_points = wire_points
        # This attribute is set the first the Biot-Savart sum is calculated.
        self._normalized_wire_connection_vectors = None
        self.current = current
        # Set the units scales.
        self.wire_length_scale = wire_length_scale
        self.current_scale = current_scale
        self.observer_length_scale = observer_length_scale
        self.b_field_scale = b_field_scale

    @property
    def wire_points(self) -> np.ndarray:
        """
        Getter method for the wire points.

        :return: The wire points describing the piecewise straight wire.
        """
        return self._wire_points

    @wire_points.setter
    def wire_points(self, new_wire_points: np.ndarray) -> None:
        """
        Set the value of the wire points. Additionally, it also resets the normalized connection vectors, indicating
        that they need to be recalculated.

        :param new_wire_points: New wire points which define the shape of the wire.
        :return: None.
        """
        self._wire_points = new_wire_points
        self._normalized_wire_connection_vectors = None

    @staticmethod
    def _calculate_normalized_connection_vectors(wire_points: np.ndarray) -> np.ndarray:
        """
        Calculates the normalized connections vectors from an array of vectors with shape (n, 3). The calculation
        performed on the individual elements is (r_i+1 - r_i)/|r_i+1- r_i|. This method is static for easier testing.

        :param wire_points: Numpy array of the shape (n ,3) representing s piecewise straight wire.
        :return np.ndarray: Numpy array containing the normalized connection vectors of all the wire points. This array
            has the shape (n-1, 3). It is needed to calculate the B field later on.
        """
        # Shift the wire points row wise. The formerly second entry is now the first one.
        shifted_wire_points = np.roll(wire_points, -1, axis=0)
        # The connection vectors are calculated by simply subtracting the wire points and the shifted wire points and
        # then taking the norm. Additionally, the last element is dropped since it is not needed.
        wire_connection_vectors = (shifted_wire_points - wire_points)[:-1]
        # Calculate the normalized connection vectors.
        normalized_wire_vectors = wire_connection_vectors / np.linalg.norm(wire_connection_vectors, axis=1)[:,
                                                                                                            np.newaxis]
        return normalized_wire_vectors

    @staticmethod
    def _calculate_unit_vectors(wire_points: np.ndarray, position: np.ndarray) -> tuple:
        """
        This method takes as input a list a numpy array defining a list of wire points in 3D space (shape is (n, 3))
        and a list of observer positions in 3D space (shape is (m, 3)). The method calculates all the normalized
        connection vectors from all points at which a wire starts to all observers. It also calculates the normalized
        vectors from all wire ending vectors to all observers. Both of these arrays have the shape (m, n-1, 3).

        :param wire_points: Numpy array defining wire points in 3D space. The array should have the shape (n, 3).
        :param position: Numpy array defining the observer positions in 3D space. this array should have the shape
            (m, 3).
        :return: Returns a tuple consisting of three elements:
            1. The normalized connection vectors from the starting wire points to each point of an observer. These
               vectors are a numpy arrays with the shape (m, n-1, 3).
            2. The normalized connection vectors from the ending wire points to each point of an observer. These
               vectors are a numpy arrays with the shape (m, n-1, 3).
            3. The distances from each starting wire point to each observer. This is a represented by a numpy array
               with the shape (m, n-1, 1).
        """
        # Connecting vectors of the wire points and the observers.
        wire_point_observer_connections = position[:, np.newaxis] - wire_points
        # Distances between all the wire points and all the observers.
        wire_points_observer_distance = np.linalg.norm(wire_point_observer_connections, axis=2)[:, :, np.newaxis]
        # Normalized connection vectors between all the wire points and all the observers.
        unit_vectors = (wire_point_observer_connections / wire_points_observer_distance)
        # All the wire points at which a wire starts or ends.
        start_point_unit_vectors = np.delete(unit_vectors, -1, axis=1)
        end_point_unit_vectors = np.delete(unit_vectors, 0, axis=1)
        # Distances between all starting wire points and all observers.
        starting_wire_points_observer_distances = np.delete(wire_points_observer_distance, -1, axis=1)
        return start_point_unit_vectors, end_point_unit_vectors, starting_wire_points_observer_distances

    @staticmethod
    def _scalar_product(array_1: np.ndarray, array_2: np.ndarray) -> np.ndarray:
        """
        Represents a scalar product of two arrays with weird shapes. array_1 has the shapes (n, 3) and array_2 has the
        shape (m, n, 3). The scalar product is taken along axis 1 of the first array and axis 2 of the second array.

        :param array_1: Numpy array with the shape (m, n, 3).
        :param array_2: Numpy array with the shape (m, n, 3).
        :return: The scalar product of array_1 and array_2 in form of a numpy array.
        """
        return np.sum(array_1 * array_2, axis=2)

    def calculate_b_field_at(self, position: np.ndarray) -> np.ndarray:
        r"""
        #TODO: Clean up this function, it is very bloated at the moment.
        Calculates the magnetic induction field of the specified wire at the wanted position. The B-field is calculated
        in the units specified in the constructor. The B-field is calculated using the formula

        .. math::
            \bm{B}(\bm{r})\approx\frac{\mu_{0}I}{4\pi}\sum\limits_{i=1}^{N}
            \frac{(\bm{e}_{i+1}-\bm{e}_{i})\cdot\bm{w}_{i}}
            {r_{i}(1-\left(\bm{e}_{i}\cdot\bm{w}_{i}\right)^{2})}(\bm{e}_{i}\times\bm{w}_{i}), \quad
            \bm{w}_{i}=\frac{\bm{r}_{i+1}-\bm{r}_{i}}{r_{i}}=\frac{\bm{r}_{i+1}-\bm{r}_{i}}{|\bm{r}_{i+1}-\bm{r}_{i}|},
            \;\bm{e}_{i}=\frac{\bm{r}-\bm{r}_{i}}{|\bm{r}-\bm{r}_{i}|},

        where :math:`\bm{B}` is the magnetic induction field, :math:`\bm{r}` is the position of the observer,
        :math:`\bm{r}_{i}` is the i-th corner point of the piecewise straight wire, :math:`\mu_{0}` is the
        magnetic constant and :math:`I` the current flowing through the wire.

        :Example:

        >>> # Positions of the observers. The rows contain the x-,y- and z-coordinates of the observers.
        >>> observers = np.array([1, 0, -1, 2], [1, 1, 0, -1], [0, -1, 2, 1])
        >>> # Resulting magnetic induction field
        >>> b_field = wire.calculate_b_field_at(observers)

        :param position: Numpy array representing one or many observers. Its shape has to be (3,) or (3, n) with an
            arbitrary integer n. Teh first rows contains all the x-coordinates of the observers, the second and third
            row contain the y- and z-coordinates.
        :return: The B-field at the given position or positions. Since the B-field is a vector field, its shape
            corresponds to the shape of the position vector.
        """
        # The following code block checks if the passed array of positions is 1D. If it is the 1D array is wrapped in a
        # 2D array with only one row and the b field is calculated as if it was a 2D array.
        # The position_flag variable is there to return the output also as a 1D array.
        position_flag = False
        if position.ndim == 1:
            position_flag = True
            position = position.reshape((position.shape[0], 1))

        # The field transposed because this makes the calculation easier.
        position = position.T
        # Check if the normalized wire connections vectors have been calculated if not calculate them.
        # These don't need to be calculated every time a magnetic field is examined if the wire didn't change its shape.
        if self._normalized_wire_connection_vectors is None:
            self._normalized_wire_connection_vectors = self._calculate_normalized_connection_vectors(self._wire_points)
        # Calculate the factor in front of the Biot-Savart sum
        biot_savart_factor = (self.current_scale / (self.b_field_scale * self.wire_length_scale)) * \
            mu_0 * self.current / (4 * pi)
        # This is the actual calculation of the magnetic induction field. All the variable names are taken from the
        # physical equation.
        # Set the length scale of the observer to agree with the one of the wire length.
        position *= (self.observer_length_scale / self.wire_length_scale)
        # Save the normalized wire connection vectors as w, which is also used in the formula.
        w = self._normalized_wire_connection_vectors[np.newaxis, :, :]
        # Save all the connecting unit vectors e_1, e_2 and the distances r, between wire points and the observers.
        e_1, e_2, r = self._calculate_unit_vectors(self.wire_points, position)
        scalar_product_1 = self._scalar_product((e_2 - e_1), w)[:, :, np.newaxis]
        scalar_product_2 = self._scalar_product(e_1, w)[:, :, np.newaxis]
        cross_product = np.cross(e_1, w)
        counter = scalar_product_1 * cross_product
        denominator = r * (1 - scalar_product_2 ** 2)
        b_field = biot_savart_factor * np.sum(counter / denominator, axis=1)
        # If the passed position was 1D, reshape the result to also be a 1D array.
        if position_flag:
            b_field = b_field[0]
        else:
            # Transpose the field so that it has the same shape as the observers.
            b_field = b_field.T
        return b_field


def save(obj: Wire, link_to_dumping_file: str) -> None:
    """
    This function saves a wire object in a pickled format in the provided file.

    :param obj: Wire object you want to save
    :param link_to_dumping_file: Link to the file in which you want to save the wire object.
    :return: None
    """
    with open(link_to_dumping_file, "wb") as file:
        pkl.dump(obj, file)
    print("Object successfully saved.")


def load(link_to_file: str) -> Wire:
    """
    Loads a pickled object from the specified file.

    :param link_to_file: Link to the file in which a pickled Wire object is stored
    :return: Wire object stored in the pickled file.
    """
    with open(link_to_file, "rb") as file:
        obj = pkl.load(file)
        return obj

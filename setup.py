"""
Setup file of the package.
"""

from setuptools import setup, find_packages
from magopt import __version__, __author__, __project__

with open("README.md", "r") as file:
    long_description = file.read()

setup(
    name=__project__,
    version=__version__,
    packages=find_packages(include=["magopt", "magopt.*"]),
    description="This package is made for the examination of the B-field of piecewise straight wires.",
    longdescription=long_description,
    author=__author__,
    author_email="tobias_alexander.liebmann@tu-darmstadt.de",
    url="https://git.rwth-aachen.de/tobias_alexander.liebmann/controling-multipole-moments-of-magnetic-chip-traps/",
    install_requires=["numpy", "matplotlib", "scipy"],
    python_requries="==3.8"
)

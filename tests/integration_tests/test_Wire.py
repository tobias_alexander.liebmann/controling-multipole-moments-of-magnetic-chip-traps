"""
:Date of origin: 23.02.2021
:Authors: Tobias Liebmann

This file contains the integration tests for the Wire class.
"""

import unittest as ut
import numpy as np
from magopt.magnetic_field_calculation import Wire


class WireTest(ut.TestCase):

    def setUp(self) -> None:
        """
        Sets up a Wire object for testing.

        :return: None.
        """
        wire_points = np.array([[0, 0, -1], [0, 0, 1]])
        current = 1

        self.test_wire = Wire(wire_points, current)

    def test_calculate_b_field_multiple_observers(self) -> None:
        """
        Tests the calculate_b_field method of the Wire class with a 2D array.

        :return: None
        """
        positions = np.array([[1., -1], [0., 0.], [0., 0.]])
        b_field_result = np.array([[0., 0.], [7.071e-7, -7.071e-7], [0., 0.]])

        b_field = self.test_wire.calculate_b_field_at(positions)

        np.testing.assert_array_almost_equal(b_field_result, b_field)

    def test_calculate_b_field_one_observer(self) -> None:
        """
        Tests The calculate_b_field method of the Wire class with a 1D array.

        :return: None
        """
        positions = np.array([1., 0., 0.])
        b_field_result = np.array([0, -7.071e-7, 0])

        b_field = self.test_wire.calculate_b_field_at(positions)

        np.testing.assert_array_almost_equal(b_field_result, b_field)


if __name__ == "__main__":
    ut.main()

"""
:Date of origin: 19.01.2022
:Authors: Tobias Liebmann

This file contains the integration tests for the BFieldAnalyzer class
"""

import unittest as ut
from magopt.magnetic_field_analyzer import BFieldAnalyzer
import numpy as np


class BFieldAnalyzerIntegrationTestCase(ut.TestCase):
    """
    Integration tests of the BFieldAnalyzer class.
    """
    def setUp(self) -> None:
        """

        :return:
        """
        def b_field(position: np.ndarray):
            """
            Example function simulating a magnetic induction field. It just returns the given position.

            :param position: A numpy array representing a position in space.
            :return: The magnetic induction field at the given position.
            """
            return position**2

        def magnitude_of_b(position: np.ndarray):
            """
            The magnitude of the magnetic induction field example function.

            :param position:
            :return:
            """
            return np.linalg.norm(b_field(position))

        self.analyzer = BFieldAnalyzer(b_field, magnitude_of_b)

    def test_find_minimum_along_z_axis(self):
        """
        Test if the find_minimum_along_z axis method of the BFieldAnalyzer class works.

        :return: None
        """
        starting_point = 10.

        minimum_position, function_value, _ = self.analyzer.find_minimum_along_z_axis(starting_point)

        self.assertAlmostEqual(0., function_value)
        self.assertAlmostEqual(0., minimum_position)


if __name__ == '__main__':
    ut.main()

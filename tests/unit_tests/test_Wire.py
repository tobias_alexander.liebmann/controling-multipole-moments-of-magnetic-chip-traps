"""
:Date of origin: 16.12.2021
:Authors: Tobias Liebmann

This file contains unit tests for the Wire class.
"""

import unittest as ut
import numpy as np
from magopt.magnetic_field_calculation import Wire


class WireTest(ut.TestCase):

    def setUp(self) -> None:
        """
        Sets up a Wire object for testing.

        :return: None.
        """
        wire_points = np.array([[0, 0, -1], [0, 0, 1]])
        current = 1

        self.test_wire = Wire(wire_points, current)

    def test_wire_points_property(self) -> None:
        """
        Tests the property wire_points of the Wire class.

        :return: None
        """
        new_wire_points = np.array([[-1, 0, 0], [1, 0, 0]])

        self.test_wire.wire_points = new_wire_points

        self.assertIsNone(self.test_wire._normalized_wire_connection_vectors)
        np.testing.assert_array_almost_equal(self.test_wire.wire_points, new_wire_points)

    def test_calculate_normalized_connection_vectors(self) -> None:
        """
        Tests the calculate_normalized_connection_vectors method of the Wire class.

        :return: None.
        """
        wire_points = np.array([[-1, 0, 0], [1, 0, 0]])
        normalized_connection_wire_points = self.test_wire._calculate_normalized_connection_vectors(wire_points)

        normalized_connection_wire_points_test = np.array([[1, 0, 0]])

        np.testing.assert_array_almost_equal(normalized_connection_wire_points_test, normalized_connection_wire_points)

    def test_scalar_product(self) -> None:
        """
        Tests the _scalar_product method of the Wire class.

        :return: None
        """
        array_1 = np.array([[[1., 1., 1.],
                             [1., 1., 1.]]])
        array_2 = np.array([[[0., 0., 0.],
                             [1., 1., 1.]],
                            [[1., 1., 1.],
                             [1., 1., 1.]]])

        dot_product = self.test_wire._scalar_product(array_1, array_2)
        dot_product_result = np.array([[0., 3.], [3., 3.]])

        np.testing.assert_array_almost_equal(dot_product_result, dot_product)

    def test_calculate_unit_vectors(self):
        """
        Tests the _calculate_unit_vectors method of the Wire class.

        :return: None.
        """
        wire_points = np.array([[0, 0, -1],
                                [0, 0, 1]])
        observers = np.array([[1, 0, 0],
                              [-1, 0, 0],
                              [0, 1, 0]])

        e_1, e_2, r = self.test_wire._calculate_unit_vectors(wire_points, observers)
        e_1_result = np.array([[[1, 0, 1]],
                               [[-1, 0, 1]],
                               [[0, 1, 1]]])/np.sqrt(2)
        e_2_result = np.array([[[1, 0, -1]],
                               [[-1, 0, -1]],
                               [[0, 1, -1]]])/np.sqrt(2)
        r_result = np.sqrt(np.array([[[2]], [[2]], [[2]]]))

        np.testing.assert_array_almost_equal(e_1_result, e_1)
        np.testing.assert_array_almost_equal(e_2_result, e_2)
        np.testing.assert_array_almost_equal(r_result, r)


if __name__ == "__main__":
    ut.main()

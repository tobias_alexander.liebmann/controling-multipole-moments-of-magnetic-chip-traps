"""
:Date of origin: 17.01.2021
:Authors: Tobias Liebmann

This file contains unit tests for the BFieldAnalyzer class.
"""

import unittest as ut
import numpy as np
from magopt.magnetic_field_analyzer import BFieldAnalyzer


class BFieldAnalyzerTest(ut.TestCase):
    """
    Test case for the BFieldAnalyzer class.
    """
    def setUp(self) -> None:
        """

        :return:
        """
        def b_field(position: np.ndarray):
            """
            Example function simulating a magnetic induction field. It just returns the given position.

            :param position: A numpy array representing a position in space.
            :return: The magnetic induction field at the given position.
            """
            return position**2

        def magnitude_of_b(position: np.ndarray):
            """
            The magnitude of the magnetic induction field example function.

            :param position:
            :return:
            """
            return np.linalg.norm(b_field(position))

        self.analyzer = BFieldAnalyzer(b_field, magnitude_of_b)

    def test_find_minimum(self):
        """
        Tests if the find_minimum method of the BFieldAnalyzer class works.

        :return: None.
        """
        starting_position = np.array([1., 1., 1.])

        minimum_position, function_value, _ = self.analyzer.find_minimum(starting_position)

        np.testing.assert_almost_equal(0., function_value)
        np.testing.assert_array_almost_equal([0., 0., 0.], minimum_position)

    def test_calculate_hessian(self):
        """
        Tests the calculate_hessian method of the BFieldAnalyzer class.

        :return: None
        """
        position = np.array([1., 0., 0.])

        hessian = self.analyzer.calculate_hessian_at(position)
        correct_hessian = np.array([[2., 0., 0.],
                                    [0., 0., 0.],
                                    [0., 0., 0.]])

        np.testing.assert_array_almost_equal(correct_hessian, hessian)

    def test_calculate_eigensystem(self):
        """
        Tests the calculate_eigensystem method of the BFieldAnalyzer class.

        :return: None
        """
        matrix = np.array([[1, 0, 1],
                           [0, 1, 0],
                           [1, 0, 1]])

        # Calculate the eigenvalues and -vectors.
        eigenvalues, eigenvectors = self.analyzer.calculate_eigensystem(matrix)

        # Define the correct eigenvalues and eigenvectors against which the other ones are compared
        correct_eigenvalues = np.array([0, 1, 2])
        correct_eigenvectors = np.array([[-1/np.sqrt(2), 0, -1/np.sqrt(2)],
                                        [0, -1, 0],
                                        [1/np.sqrt(2), 0, -1/np.sqrt(2)]])

        np.testing.assert_array_almost_equal(correct_eigenvalues, eigenvalues)
        np.testing.assert_array_almost_equal(correct_eigenvectors, eigenvectors)

    def test_magnitude_of_b_z_component(self):
        """
        Tests the helper function _magnitude_of_b_z_component of the BFieldAnalyzer class

        :return: None
        """
        # TODO: Maybe put this test in the test_find_minimum_along_z_axis method.
        position_z = 0

        magnitude_of_b = self.analyzer._magnitude_of_b_z_component(position_z)

        self.assertAlmostEqual(0., magnitude_of_b)

    def test_calculate_multipole_moment(self):
        """
        Test the calculate_multipole_moment method by calculating the multipole moments for l={1, 2, 3} and all the
        corresponding values of m from -l to l and compares them to previously computed multipole moments of the same
        system.

        :return: None
        """
        sphere_mid_point = np.array([0., 0., 0.])
        integration_radius = 1
        correct_multipole_moments = np.array([0.8683215055 + 0.8683215055j, 1.227992050, -0.8683215055 +
                                              0.8683215055j, 0., 0., 0., 0., 0., 0.09986646089 - 0.09986646089j,
                                              0., -0.07735622798 - 0.07735622798j, 0.1786465562,
                                              0.07735622798 - 0.07735622798j, 0, -0.09986646089 - 0.09986646089j])

        multipole_moments = np.array([self.analyzer.calculate_multipole_moment(l, m, integration_radius,
                                                                               sphere_mid_point) for l
                                      in range(1, 4) for m in range(-l, l+1)])

        np.testing.assert_array_almost_equal(correct_multipole_moments, multipole_moments)


if __name__ == "__main__":
    ut.main()
